﻿using NUnit.Framework;
using Tesserae.Services;

namespace NUnitTest
{
    public class RatingServiceTest
    {
        private IRatingService service = new RatingServiceEF();

        [SetUp]
        public void Init()
        {
            service.ClearRatings();
        }

        [Test]
        public void SetRatingTest()
        {
            service.SetRating(new Rating("Viktor", 5));
            var rating = service.GetRating("Viktor");
            Assert.AreEqual(5, rating);

            service.SetRating(new Rating("Viktor", 2));
            rating = service.GetRating("Viktor");
            Assert.AreEqual(2, rating);
        }

        [Test]
        public void ClearRatingsTest()
        {
            service.SetRating(new Rating("Viktor", 5));
            service.SetRating(new Rating("Mato", 3));
            service.SetRating(new Rating("Pato", 3));
            service.SetRating(new Rating("Simon", 4));

            service.ClearRatings();

            Assert.AreEqual(0, service.GetRating("Viktor"));
            Assert.AreEqual(0, service.GetRating("Robo"));
        }

        [Test]
        public void GetAvarageRatingTest()
        {
            service.SetRating(new Rating("Viktor", 5));
            service.SetRating(new Rating("Mato", 3));
            service.SetRating(new Rating("Pato", 3));
            service.SetRating(new Rating("Simon", 4));
            service.SetRating(new Rating("Anton", 4));


            Assert.AreEqual("3,80", string.Format("{0:0.00}", service.GetAvarageRating()));
        }

        [Test]
        public void GetRatingsTest()
        {
            service.SetRating(new Rating("Viktor", 5));
            service.SetRating(new Rating("Mato", 1));
            service.SetRating(new Rating("Pato", 3));


            var rating = service.GetRating("Viktor");
            Assert.AreEqual(5, rating);

            rating = service.GetRating("Mato");
            Assert.AreEqual(1, rating);

            rating = service.GetRating("Pato");
            Assert.AreEqual(3, rating);
        }
    }
}