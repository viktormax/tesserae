﻿using NUnit.Framework;
using Tesserae;

namespace NUnitTest
{
    public class GameLogicTest
    {
        private readonly Tile[,] tiles = new Tile[,] {
            /*     0            1            2            3            4      */
       /*0*/{ new Tile(1), new Tile(2), new Tile(3), new Tile(2), new Tile(1) },
       /*1*/{ new Tile(4), new Tile(5), new Tile(0), new Tile(2), new Tile(2) },
       /*2*/{ new Tile(6), new Tile(8), new Tile(7), new Tile(0), new Tile(7) },
       /*3*/{ new Tile(1), new Tile(8), new Tile(1), new Tile(5), new Tile(7) },
       /*4*/{ new Tile(6), new Tile(4), new Tile(7), new Tile(5), new Tile(0) },
        };

        private Board board = new Board(MapType.BIG_RACTANGLE, Complexity.BEGINNER);

        [SetUp]
        public void Init()
        {
            board.Tiles = tiles;
            board.Width = 5;
            board.Height = 5;
        }

        [Test]
        public void DoNotJumpOverNullTileTest()
        {
            Move m = board.Validate(new Position(3, 0), new Position(3, 2));
            Assert.IsNull(m);

            m = board.Validate(new Position(3, 0), new Position(1, 2));
            Assert.IsNull(m);
        }

        [Test]
        public void DoNotMoveEmptyTyleTest()
        {
            var moves = board.GetMoves(new Position(1, 2));
            Assert.IsEmpty(moves);

            moves = board.GetMoves(new Position(2, 3));
            Assert.IsEmpty(moves);
        }

        [Test]
        public void DoNotMOveNullTileTest()
        {
            var moves = board.GetMoves(new Position(2, 1));
            Assert.IsEmpty(moves);
        }

        [Test]
        public void AutOfBandsTest()
        {
            var moves = board.GetMoves(new Position(5, 5));
            Assert.IsEmpty(moves);
        }
    }
}
