﻿using NUnit.Framework;
using Tesserae.Services;

namespace NUnitTest
{
    public class CommentServiceTest
    {
        private ICommentService service = new CommentServiceEF();

        [SetUp]
        public void Init()
        {
            service.ClearComments();
        }

        [Test]
        public void AddCommentTest()
        {
            service.AddComment(new Comment("Viktor", "This game is awesome"));

            var comments = service.GetComments();

            Assert.AreEqual(1, comments.Count);
            Assert.AreEqual("Viktor", comments[0].Player);
            Assert.AreEqual("This game is awesome", comments[0].Content);
        }

        [Test]
        public void ClearComentsTest()
        {
            service.AddComment(new Comment("Viktor", "This game is awesome"));
            service.AddComment(new Comment("Mato", "Completely agree with you Viktor"));
            service.AddComment(new Comment("Anonymous", "Matp is asshole"));

            var comments = service.GetComments();

            Assert.AreEqual(3, comments.Count);

            service.ClearComments();

            comments = service.GetComments();

            Assert.AreEqual(0, comments.Count);
        }

        [Test]
        public void DeleteComentTest()
        {
            service.AddComment(new Comment("Viktor", "This game is awesome"));

            var comments = service.GetComments();

            service.DeleteComment(comments[0].Id);

            comments = service.GetComments();

            Assert.AreEqual(0, comments.Count);
        }

        [Test]
        public void GetCommentsTest()
        {
            service.AddComment(new Comment("Viktor", "This game is awesome"));
            service.AddComment(new Comment("Mato", "Completely agree with you Viktor"));
            service.AddComment(new Comment("Peter", "Not so good"));
            service.AddComment(new Comment("Lisa", "This game is too difficult"));

            var comments = service.GetComments();

            Assert.AreEqual(4, comments.Count);
            Assert.AreEqual("Viktor", comments[3].Player);
            Assert.AreEqual("This game is awesome", comments[3].Content);

            Assert.AreEqual("Mato", comments[2].Player);
            Assert.AreEqual("Completely agree with you Viktor", comments[2].Content);

            Assert.AreEqual("Peter", comments[1].Player);
            Assert.AreEqual("Not so good", comments[1].Content);

            Assert.AreEqual("Lisa", comments[0].Player);
            Assert.AreEqual("This game is too difficult", comments[0].Content);
        }
    }
}