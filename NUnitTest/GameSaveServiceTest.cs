﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;
using Tesserae;
using Tesserae.Services;

namespace NUnitTest
{
    class GameSaveServiceTest
    {
        private Board board;
        private History history;
        private string name = "12345678098765432";
        IGameSaveService gameSaveService = new GameSaveServiceEF();

        [SetUp]
        public void Init()
        {
            board = new Board(MapType.BIG_CIRCLE, Complexity.ADVANCED);
            history = new History();
            
        }



        [Test]
        public void DeleteGame()
        {
            gameSaveService.SaveGame(new GameSave(name, "game", board, history));
            history.AddMove(new Move(new Position(1, 1), new Position(3, 3)));
            gameSaveService.SaveGame(new GameSave(name, "new", board, history));
        
            gameSaveService.DeleteSave(name, "game");
            gameSaveService.DeleteSave(name, "new");
            var saves = gameSaveService.GetSaves(name);
        
            Assert.AreEqual(0, saves.Count);
        }

        [Test]
        public void SaveGame()
        {
            gameSaveService.SaveGame(new GameSave(name, "game", board, history));
            gameSaveService.SaveGame(new GameSave(name, "new", board, history));

            var saves = gameSaveService.GetSaves(name);
            gameSaveService.DeleteSave(name, "game");
            gameSaveService.DeleteSave(name, "new");

            Assert.AreEqual(2, saves.Count);
        }

        [Test]
        public void ReSaveGame()
        {
            gameSaveService.SaveGame(new GameSave(name, "game", board, history));
            history.AddMove(new Move(new Position(1, 1), new Position(3, 3)));
            gameSaveService.SaveGame(new GameSave(name, "game", board, history));

            var saves = gameSaveService.GetSaves(name);
            gameSaveService.DeleteSave(name, "game");

            Assert.AreEqual(1, saves.Count);
        }

        [Test]
        public void GetGames()
        {
            gameSaveService.SaveGame(new GameSave(name, "game", board, history));
            history.AddMove(new Move(new Position(1, 1), new Position(3, 3)));
            gameSaveService.SaveGame(new GameSave(name, "game", board, history));
            gameSaveService.SaveGame(new GameSave("jaro" + name, "game", board, history));
            gameSaveService.SaveGame(new GameSave(name, "hop", board, history));
            gameSaveService.SaveGame(new GameSave("domcek" + name, "ups", board, history));
            
            var saves = gameSaveService.GetSaves(name);
            Assert.AreEqual(2, saves.Count);

            saves = gameSaveService.GetSaves("jaro" + name);
            Assert.AreEqual(1, saves.Count);

            saves = gameSaveService.GetSaves("domcek" + name);
            Assert.AreEqual(1, saves.Count);

            gameSaveService.DeleteSave(name, "game");
            gameSaveService.DeleteSave("jaro" + name, "game");
            gameSaveService.DeleteSave(name, "hop");
            gameSaveService.DeleteSave("domcek" + name, "ups");
        }
    }
}
