using NUnit.Framework;
using System.Collections.Generic;
using Tesserae;
using Tesserae.Services;

namespace NUnitTest
{
    public class ScoreServiceTest
    {
        private IScoreService service = new ScoreServiceList();
        private Board board = new Board(MapType.BIG_CIRCLE, Complexity.ADVANCED);
        private History history = new History();

        [SetUp]
        public void Init()
        {
            service.ClearScores();
        }

        [Test]
        public void ClearScoresTest()
        {
            service.AddScore(new Score("Janko", history, board));
            service.AddScore(new Score("Evka", history, board));

            service.ClearScores();

            Assert.AreEqual(0, service.GetTopScores().Count);
        }

        [Test]
        public void AddScoreTest()
        {
            service.AddScore(new Score("Viktor", history, board));

            var scores = service.GetTopScores();

            Assert.AreEqual(1, scores.Count);
            Assert.AreEqual("Viktor", scores[0].Player);
            Assert.AreEqual(Complexity.ADVANCED, scores[0].Complexity);
            Assert.AreEqual(MapType.BIG_CIRCLE, scores[0].Map);
        }

        [Test]
        public void ScoreExceptionTest()
        {
         //   Score score = new Score(null, 5, 7, Complexity.ADVANCED, MapType.RACTANGLE);
         //   Assert.Catch<ScoreException>(() => service.AddScore(score));

            Assert.Catch<ScoreException>(() => service.AddScore(null));

        //    score = new Score("Max", 5, 7, Complexity.ADVANCED, MapType.EIGHT);
        //   Assert.DoesNotThrow(() => service.AddScore(score));
        }

        [Test]
        public void TopScoreTest()
        {
            service.AddScore(new Score("Janko", history, board));
            service.AddScore(new Score("Evka", history, board));
            service.AddScore(new Score("Vasa", history, board));
            service.AddScore(new Score("Ola", history, board));
            service.AddScore(new Score("Janko", history, board));
            service.AddScore(new Score("Evka", history, board));
            service.AddScore(new Score("Vasa", history, board));
            service.AddScore(new Score("Ola", history, board));
            service.AddScore(new Score("Janko", history, board));
            service.AddScore(new Score("Evka", history, board));
            service.AddScore(new Score("Vasa", history, board));
            service.AddScore(new Score("Ola", history, board));

            IList<Score> scores = service.GetTopScores();
        }
    }
}