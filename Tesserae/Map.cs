﻿namespace Tesserae
{
    public static class Map
    {
        private static readonly byte[,] infinity = new byte[,]
        {
            {0,0,1,1,1,0,0,0,1,1,1,0,0},
            {0,1,1,1,1,1,0,1,1,1,1,1,0},
            {1,1,1,1,1,1,1,1,1,1,1,1,1},
            {1,1,1,1,1,1,1,1,1,1,1,1,1},
            {1,1,1,1,1,1,1,1,1,1,1,1,1},
            {1,1,1,1,1,1,1,1,1,1,1,1,1},
            {0,1,1,1,1,1,0,1,1,1,1,1,0},
            {0,0,1,1,1,0,0,0,1,1,1,0,0},
        };

        private static readonly byte[,] hexagon = new byte[,]
        {
            {0,0,0,1,1,1,1,1,0,0,0},
            {0,0,1,1,1,1,1,1,1,0,0},
            {0,1,1,1,1,1,1,1,1,1,0},
            {1,1,1,1,1,1,1,1,1,1,1},
            {1,1,1,1,1,1,1,1,1,1,1},
            {0,1,1,1,1,1,1,1,1,1,0},
            {0,0,1,1,1,1,1,1,1,0,0},
            {0,0,0,1,1,1,1,1,0,0,0},
        };

        private static readonly byte[,] eight = new byte[,]
        {
            {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
            {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
            {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
            {1,1,1,0,0,1,1,1,1,1,0,0,1,1,1},
            {1,1,1,0,0,1,1,1,1,1,0,0,1,1,1},
            {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
            {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
            {1,1,1,1,1,1,1,1,1,1,1,1,1,1,1},
        };

        private static readonly byte[,] bigcircle = new byte[,]
        {
            {0,0,1,1,1,1,1,1,0,0},
            {0,1,1,1,1,1,1,1,1,0},
            {1,1,1,1,1,1,1,1,1,1},
            {1,1,1,1,1,1,1,1,1,1},
            {1,1,1,1,1,1,1,1,1,1},
            {1,1,1,1,1,1,1,1,1,1},
            {1,1,1,1,1,1,1,1,1,1},
            {1,1,1,1,1,1,1,1,1,1},
            {0,1,1,1,1,1,1,1,1,0},
            {0,0,1,1,1,1,1,1,0,0},
        };

        private static readonly byte[,] circle = new byte[,]
        {
            {0,0,1,1,1,1,0,0},
            {0,1,1,1,1,1,1,0},
            {1,1,1,1,1,1,1,1},
            {1,1,1,1,1,1,1,1},
            {1,1,1,1,1,1,1,1},
            {1,1,1,1,1,1,1,1},
            {0,1,1,1,1,1,1,0},
            {0,0,1,1,1,1,0,0},
        };

        private static readonly byte[,] x = new byte[,]
        {
            {1,1,1,1,0,0,0,1,1,1,1},
            {1,1,1,1,1,0,1,1,1,1,1},
            {0,1,1,1,1,1,1,1,1,1,0},
            {0,0,1,1,1,1,1,1,1,0,0},
            {0,1,1,1,1,1,1,1,1,1,0},
            {1,1,1,1,1,0,1,1,1,1,1},
            {1,1,1,1,0,0,0,1,1,1,1},
        };

        private static readonly byte[,] zero = new byte[,]
        {
            {0,0,1,1,1,1,1,1,1,1,0,0},
            {0,1,1,1,1,1,1,1,1,1,1,0},
            {1,1,1,1,1,1,1,1,1,1,1,1},
            {1,1,1,1,1,0,0,1,1,1,1,1},
            {1,1,1,1,1,0,0,1,1,1,1,1},
            {1,1,1,1,1,1,1,1,1,1,1,1},
            {0,1,1,1,1,1,1,1,1,1,1,0},
            {0,0,1,1,1,1,1,1,1,1,0,0},
        };

        public static byte[,] GetMap(MapType type)
        {
            switch (type)
            {
                case MapType.ZERO:
                    return zero;
                case MapType.X:
                    return x;
                case MapType.RACTANGLE:
                    return GetMap(6, 8);
                case MapType.BIG_RACTANGLE:
                    return GetMap(6, 12);
                case MapType.CIRCLE:
                    return circle;
                case MapType.BIG_CIRCLE:
                    return bigcircle;
                case MapType.EIGHT:
                    return eight;
                case MapType.HEXAGON:
                    return hexagon;
                case MapType.INFINITY:
                    return infinity;
                default: return null;
            }
        }

        private static byte[,] GetMap(int row, int col)
        {
            if (row < 5 || col < 5 || row > 20 || col > 20)
                return null;
            var ret = new byte[row, col];
            for (var i = 0; i < row; i++)
                for (var j = 0; j < col; j++)
                    ret[i, j] = 1;
            return ret;
        }

        public static int GetTilesNumber(MapType type)
        {
            var map = GetMap(type);
            int tileNumber = 0;
            for (var i = 0; i < map.GetLength(0); i++)
                for (var j = 0; j < map.GetLength(1); j++)
                    if (map[i, j] == 1)
                        tileNumber++;
            return tileNumber;
        }

        public static int GetHeight(MapType type)
        {
            return GetMap(type).GetLength(0);
        }

        public static int GetWidth(MapType type)
        {
            return GetMap(type).GetLength(1);
        }
    }
}
