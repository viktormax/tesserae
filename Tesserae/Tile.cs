﻿using System;

namespace Tesserae
{
    [Serializable]
    public class Tile
    {
        public byte Value { get; set; }

        public Tile() { }

        public Tile(byte type = 0)
        {
            Value = type;
        }

        public bool IsPrimary()
        {
            return Value == 1 || Value == 2 || Value == 4;
        }

        public bool IsSecondary()
        {
            return Value == 3 || Value == 5 || Value == 6;
        }

        public bool IsTertiary()
        {
            return Value == 7;
        }

        public bool IsEmpty()
        {
            return Value == 0;
        }

        public bool Contains(Tile tile)
        {
            return (Value & tile.Value) == tile.Value;
        }

        public bool DontContain(Tile tile)
        {
            return (Value & tile.Value) == 0;
        }

        public bool IsEqual(Tile tile)
        {
            return Value == tile.Value;
        }
    }
}
