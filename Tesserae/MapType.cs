﻿namespace Tesserae
{
    public enum MapType
    {
        BIG_RACTANGLE,
        RACTANGLE,
        BIG_CIRCLE,
        INFINITY,
        HEXAGON,
        CIRCLE,
        EIGHT,
        ZERO,
        X
    }
}
