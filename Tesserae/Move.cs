﻿using System.ComponentModel.DataAnnotations.Schema;
using Tesserae.Services;
using Newtonsoft.Json;
using System;

namespace Tesserae
{
    [Serializable]
    public class Move
    {
        public int Id { get; set; }
        public int Order { get; set; }
        [NotMapped]
        public Position Start { get; set; }
        [NotMapped]
        public Position Middle { get; set; }
        [NotMapped]
        public Position End { get; set; }
        public string Direction { get; set; }
        public byte OldStart { get; set; }
        public byte OldMiddle { get; set; }
        public byte OldEnd { get; set; }
        //[JsonIgnore]
        public GameSave GameSave { get; set; }

        public Move() { }

        public Move(Position start, Position end)
        {
            Start = start;
            End = end;
            Middle = new Position(
                (start.Row + end.Row) / 2,
                (start.Col + end.Col) / 2
            );
        }

        public void Execute(Board board)
        {
            OldStart = board.GetValue(Start);
            OldMiddle = board.GetValue(Middle);
            OldEnd = board.GetValue(End);

            if (OldMiddle == 1 || OldMiddle == 2 || OldMiddle == 4)
                board.SetValue(Middle, 0);
            else
                board.SetValue(Middle, (byte)(board.GetValue(Middle) - board.GetValue(Start)));

            board.SetValue(End, (byte)(board.GetValue(End) | board.GetValue(Start)));
            board.SetValue(Start, 0);
        }

        public void UndoMove(Board board)
        {
            board.SetValue(Start, OldStart);
            board.SetValue(Middle, OldMiddle);
            board.SetValue(End, OldEnd);
        }
    }
}
