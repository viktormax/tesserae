﻿using System;

namespace Tesserae
{
    public class Generator
    {
        private readonly Random random;
        private readonly byte[] values;
        private readonly int tileNumber;
        private readonly int primaryNumber;
        private readonly int secondaryNumber;
        private int callNumber;

        public Generator(Complexity complex, MapType map)
        {
            random = new Random();
            tileNumber = Map.GetTilesNumber(map);
            values = new byte[tileNumber];
            callNumber = 0;

            if (map == MapType.RACTANGLE)
            {
                primaryNumber = tileNumber / 3;
            }
            else if (complex == Complexity.BEGINNER)
            {
                primaryNumber = tileNumber / 4;
                secondaryNumber = tileNumber / 12;
            }
            else if (complex == Complexity.INTERMEDIATE)
            {
                primaryNumber = tileNumber / 6;
                secondaryNumber = tileNumber / 6;
            }
            else if (complex == Complexity.ADVANCED)
            {
                primaryNumber = tileNumber / 8;
                secondaryNumber = tileNumber / 8 * 5 / 3;
            }

            Fill();
        }

        private void Fill()
        {
            var index = 0;
            for (var j = 0; j < primaryNumber; j++, index++)
                values[index] = 1;
            for (var j = 0; j < primaryNumber; j++, index++)
                values[index] = 2;
            for (var j = 0; j < primaryNumber; j++, index++)
                values[index] = 4;
            for (var j = 0; j < secondaryNumber; j++, index++)
                values[index] = 3;
            for (var j = 0; j < secondaryNumber; j++, index++)
                values[index] = 5;
            for (var j = 0; j < secondaryNumber; j++, index++)
                values[index] = 6;

            while (index < tileNumber)
            {
                if (values[index] == 0)
                    values[index] = (byte)random.Next(1, 7);
                index++;
            }
        }

        public byte GetRandom()
        {
            if (callNumber == tileNumber)
                throw new Exception("You should create a new Generator class");
            callNumber++;
            int index;

            do
                index = random.Next(tileNumber);
            while (values[index] == 0);

            var value = values[index];
            values[index] = 0;
            return value;
        }
    }
}
