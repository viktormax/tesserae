﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tesserae.Services
{
    public interface IGameSaveService
    {
        List<GameSave> GetSaves(string player);

        void DeleteSave(string player, string game);

        void SaveGame(GameSave gameSave);
    }
}
