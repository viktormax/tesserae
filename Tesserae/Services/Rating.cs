﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Tesserae.Services
{
    public class Rating
    {
        public Rating(string player, int stars)
        {
            RatedOn = DateTime.Now;
            Player = player;
            Stars = stars;
        }

        public Rating() { }

        [Key]
        public string Player { get; set; }

        public int Stars { get; set; }

        public DateTime RatedOn { get; set; }
    }
}
