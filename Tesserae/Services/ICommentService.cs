﻿using System.Collections.Generic;

namespace Tesserae.Services
{
    public interface ICommentService
    {
        void AddComment(Comment comment);

        void ClearComments();

        void DeleteComment(int id);

        IList<Comment> GetComments();
    }
}
