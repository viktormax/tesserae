﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tesserae.Services
{
    public class RatingServiceList : IRatingService
    {
        private List<Rating> ratingList = new List<Rating>();

        public void SetRating(Rating rating)
        {
            ratingList.Add(rating);
        }

        public void ClearRatings()
        {
            ratingList.Clear();
        }

        public float GetAvarageRating()
        {
            return (float)ratingList.Sum(item => item.Stars) / ratingList.Count;
        }

        public int GetRating(string player)
        {
            return ratingList.First(r => r.Player == player).Stars;
        }
    }
}
