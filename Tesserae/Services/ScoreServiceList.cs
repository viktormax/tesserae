﻿using System.Collections.Generic;
using System.Linq;

namespace Tesserae.Services
{
    public class ScoreServiceList : IScoreService
    {
        private List<Score> scoreList = new List<Score>();

        public void AddScore(Score score)
        {
            if (score == null)
                throw new ScoreException("Score must be not null");
            if (score.Player == null)
                throw new ScoreException("Score contains null Name");
            score.Id = scoreList.Count();
            scoreList.Add(score);
        }

        public void ClearScores()
        {
            scoreList.Clear();
        }

        public IList<Score> GetTopScores()
        {
            return scoreList.OrderByDescending(s => s.Points).Take(10).ToList();
        }
    }
}
