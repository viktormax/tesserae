﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Tesserae.Services
{
    public class GameSaveServiceEF : IGameSaveService
    {
        public void SaveGame(GameSave gameSave)
        {
            using (var context = new TesseraeContext())
            {
                try
                {
                    DeleteSave(gameSave.Player, gameSave.GameName);
                    context.Add(gameSave);
                    context.SaveChanges();
                }
                catch (Exception e)
                {
                    throw new GameSaveException(e.Message);
                }
            }
        }

        public void DeleteSave(string player, string game)
        {
            using (var context = new TesseraeContext())
            {
                try
                {
                    if (context.GameSaves.Any(g => g.Player == player && g.GameName == game))
                    {
                        var g = context.GameSaves
                            .Include(save => save.Moves)
                            .Where(save => save.Player == player && save.GameName == game)
                            .First();
                        context.Remove(g);
                        context.SaveChanges();
                    }
                }
                catch (Exception e)
                {
                    throw new GameSaveException(e.Message);
                }
            }
        }

        public List<GameSave> GetSaves(string player)
        {
            using (var context = new TesseraeContext())
            {
                try
                {
                    if (!context.GameSaves.Any(g => g.Player == player))
                        return new List<GameSave>();

                    return context.GameSaves
                        .Where(save => save.Player == player)
                        .Include(save => save.Moves)
                        .ToList();
                }
                catch (Exception e)
                {
                    throw new GameSaveException(e.Message);
                }                
            }
        }
    }
}
