﻿using Microsoft.EntityFrameworkCore;

namespace Tesserae.Services
{
    public class TesseraeContext : DbContext
    {
        public DbSet<Score> Scores { get; set; }

        public DbSet<Rating> Ratings { get; set; }

        public DbSet<Comment> Comments { get; set; }

        public DbSet<Move> Moves { get; set; }

        public DbSet<GameSave> GameSaves { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Server=(localdb)\mssqllocaldb;Database=Tesserae;Trusted_Connection=True;");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Move>()
                .HasOne(g => g.GameSave)
                .WithMany(m => m.Moves)
                .IsRequired()
                .OnDelete(DeleteBehavior.Cascade);
        }
    }
}

