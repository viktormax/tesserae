﻿using System;

namespace Tesserae.Services
{
    public class Score
    {
        public Score(string player, History history, Board board)
        {
            PlayedOn = DateTime.Now;
            Player = player;
            Tiles = board.TileNumber;
            Moves = history.MoveNumber;
            Complexity = board.Complexity;
            Map = board.Type;
            Points = ((1000 * ((int)Complexity + 1)) - Moves * 10) / Tiles;
            Points = Points < 1 ? 1 : Points;
        }

        public Score() { }

        public int Id { get; set; }

        public string Player { get; set; }

        public int Tiles { get; set; }

        public int Moves { get; set; }

        public Complexity Complexity { get; set; }

        public MapType Map { get; set; }

        public DateTime PlayedOn { get; set; }

        public int Points { get; set; }
    }
}
