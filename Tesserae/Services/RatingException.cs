﻿using System;

namespace Tesserae.Services
{
    public class RatingException : Exception
    {
        public RatingException()
        {
        }

        public RatingException(string message) : base(message)
        {
        }

        public RatingException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
