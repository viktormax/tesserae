﻿using System.Collections.Generic;

namespace Tesserae.Services
{
    public class CommentServiceList : ICommentService
    {
        private List<Comment> commentList = new List<Comment>();
        private int Id { get; set; }

        public void AddComment(Comment comment)
        {
            if (comment == null)
                throw new CommentException("Comment must be not null");
            if (comment.Player == null)
                throw new CommentException("Comment contains null Name");
            if (comment.Content == null)
                throw new CommentException("Comment content is empty");
            comment.Id = commentList.Count;
            commentList.Add(comment);
            Id++;
        }



        public IList<Comment> GetComments()
        {
            return commentList;
        }

        public void ClearComments()
        {
            Id = 0;
            commentList.Clear();
        }

        public void DeleteComment(int id)
        {
            var comment = commentList.Find(c => c.Id == Id);
            if (comment != null)
            {
                commentList.Remove(comment);
                Id--;
            }           
        }
    }
}
