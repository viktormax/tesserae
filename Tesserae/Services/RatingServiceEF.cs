﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Collections.Generic;

namespace Tesserae.Services
{
    public class RatingServiceEF : IRatingService
    {
        public void ClearRatings()
        {
            using (var context = new TesseraeContext())
            {
                try
                {
                    context.Database.ExecuteSqlCommand("DELETE FROM Ratings");
                }
                catch (Exception e)
                {
                    throw new RatingException(e.Message);
                }                
            }
        }

        public float GetAvarageRating()
        {
            using (var context = new TesseraeContext())
            {
                try
                {
                    return (float)(from r in context.Ratings select r.Stars).DefaultIfEmpty(0).Average();
                }
                catch (Exception e)
                {
                    throw new RatingException(e.Message);
                }                
            }
        }

        public int GetRating(string player)
        {
            using (var context = new TesseraeContext())
            {
                try
                {
                    return (from r in context.Ratings where r.Player == player select r.Stars).DefaultIfEmpty(0).Single();
                }
                catch (Exception e)
                {
                    throw new RatingException(e.Message);
                }
            }
        }

        public void SetRating(Rating rating)
        {
            using (var context = new TesseraeContext())
            {
                try
                {
                    if (context.Ratings.Any(r => r.Player == rating.Player))
                    {
                        context.Update(rating);
                    }
                    else
                    {
                        context.Add(rating);
                    }
                    context.SaveChanges();
                }
                catch (Exception e)
                {
                    throw new RatingException(e.Message);
                }
            }
        }
    }
}
