﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace Tesserae.Services
{
    [Serializable]
    public class GameSave
    {
        public int Id { get; set; }
        public string GameName { get; set; }
        public string Player { get; set; }
        public string Tiles { get; set; }
        public DateTime PlayedOn { get; set; }
        public Complexity Complexity { set; get; }
        public MapType MapType { set; get; }
        public List<Move> Moves { get; set; }

        public GameSave() { }

        public GameSave(string player, string game, Board board, History history)
        {
            GameName = game;
            PlayedOn = DateTime.Now;
            Player = player;
            Tiles = TilesToString(board);
            Complexity = board.Complexity;
            MapType = board.Type;
            Moves = history.SaveMoves();
        }

        private string TilesToString(Board board)
        {
            string ret = "";
            for (int i = 0; i < board.Height; i++)
            {
                for (int j = 0; j < board.Width; j++)
                {
                    ret += $"{board.Tiles[i,j].Value} ";
                }
                ret += "- ";
            }
            return ret;
        }

        public Board GetBoard()
        {
            int height = Map.GetHeight(MapType);
            int width = Map.GetWidth(MapType);
            Tile[,] tiles2d = new Tile[height, width];
            for (int i = 0, index = 0; i < height; i++)
            {
                for (int j = 0; j < width; j++)
                {
                    tiles2d[i, j] = new Tile((byte)(Tiles[index]-'0'));
                    index += 2;
                }
                index+=2;
            }
            return new Board(MapType, Complexity, tiles2d);
        }

        public History GetHistory()
        { 
            History h = new History();
            Moves = Moves.OrderBy(x => x.Order).ToList();
            h.LoadMoves(Moves);
            return h;
        }
    }
}
