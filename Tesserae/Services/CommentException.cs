﻿using System;

namespace Tesserae.Services
{
    public class CommentException : Exception
    {
        public CommentException()
        {
        }

        public CommentException(string message) : base(message)
        {
        }

        public CommentException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
