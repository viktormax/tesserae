﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Tesserae.Services
{
    public class ScoreServiceEF : IScoreService
    {
        public void AddScore(Score score)
        {
            using (var context = new TesseraeContext())
            {
                try
                {
                    context.Scores.Add(score);
                    context.SaveChanges();
                }
                catch (Exception e)
                {
                    throw new ScoreException(e.Message);
                }             
            }
        }

        public void ClearScores()
        {
            using (var context = new TesseraeContext())
            {
                try
                {
                    context.Database.ExecuteSqlCommand("DELETE FROM Scores");
                }
                catch (Exception e)
                {
                    throw new ScoreException(e.Message);
                }
            }
        }

        public IList<Score> GetTopScores()
        {
            using (var context = new TesseraeContext())
            {
                try
                {
                    return (from s in context.Scores orderby s.Points descending select s).Take(10).ToList(); 
                }
                catch (Exception e)
                {
                    throw new ScoreException(e.Message);
                }

            }
        }
    }
}
