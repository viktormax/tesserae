﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tesserae.Services
{
    public class GameSaveException : Exception
    {
        public GameSaveException()
        {
        }

        public GameSaveException(string message) : base(message)
        {
        }

        public GameSaveException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
