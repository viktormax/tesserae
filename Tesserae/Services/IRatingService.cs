﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tesserae.Services
{
    public interface IRatingService
    {
        void SetRating(Rating rating);

        int GetRating(string player);

        void ClearRatings();

        float GetAvarageRating();
    }
}
