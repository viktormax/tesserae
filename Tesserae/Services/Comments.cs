﻿using System;

namespace Tesserae.Services
{
    public class Comment
    {
        public Comment(string player, string content)
        {
            CommentedOn = DateTime.Now;
            Player = player;
            Content = content;
        }

        public Comment() { }

        public int Id { get; set; }

        public string Player { get; set; }

        public string Content { get; set; }

        public DateTime CommentedOn { get; set; }
    }
}
