﻿using System.Collections.Generic;

namespace Tesserae.Services
{
    public interface IScoreService
    {
        void AddScore(Score score);

        IList<Score> GetTopScores();

        void ClearScores();
    }
}
