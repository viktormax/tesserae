﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Tesserae.Services
{
    public class CommentServiceEF : ICommentService
    {
        public void AddComment(Comment comment)
        {
            using(var context = new TesseraeContext())
            {
                try
                {
                    context.Add(comment);
                    context.SaveChanges();
                }
                catch (Exception e)
                {
                    throw new CommentException(e.Message);
                }
            }
        }

        public IList<Comment> GetComments()
        {
            using(var context = new TesseraeContext())
            {
                try
                {
                    return (from c in context.Comments orderby c.CommentedOn descending select c).ToList();
                }
                catch (Exception e)
                {
                    throw new CommentException(e.Message);
                }                
            }
        }

        public void ClearComments()
        {
            using (var context = new TesseraeContext())
            {
                try
                {
                    context.Database.ExecuteSqlCommand("DELETE FROM Comments");
                }
                catch (Exception e)
                {
                    throw new CommentException(e.Message);
                }                
            }
        }

        public void DeleteComment(int id)
        {
            using (var context = new TesseraeContext())
            {
                try
                {
                    context.Database.ExecuteSqlCommand($"DELETE FROM Comments WHERE Id = {id}");
                }
                catch (Exception e)
                {
                    throw new CommentException(e.Message);
                }                
            }
        }
    }
}
