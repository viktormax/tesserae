﻿using System;
using System.Collections.Generic;

namespace Tesserae
{
    [Serializable]
    public class Board
    { 
        public Board(MapType type, Complexity complexity)
        {
            Complexity = complexity;
            Type = type;
            CreateBoard();
        }

        public Board(MapType type, Complexity complexity, Tile[,] tiles)
        {
            Complexity = complexity;
            Type = type;
            Tiles = tiles;
            Height = Map.GetHeight(type);
            Width = Map.GetWidth(type);
        }

        public Complexity Complexity { get; set; }

        public MapType Type { get; set; }

        public int Height { get; set; }

        public int Width { get; set; }

        public Tile[,] Tiles { get; set; }

        public byte GetValue(Position p)
        {
            return Tiles[p.Row, p.Col].Value;
        }

        public void SetValue(Position p, byte value)
        {
            Tiles[p.Row, p.Col].Value = value;
        }

        public int TileNumber {
            get {
                int number = 0;
                for (var i = 0; i < Height; i++)
                {
                    for (var j = 0; j < Width; j++)
                    {
                        if (Tiles[i, j] == null)
                            continue;
                        if (Tiles[i, j].Value != 0)
                            number++;
                    }
                }
                return number;
            }
        }

        private void CreateBoard()
        {
            byte[,] map = Map.GetMap(Type);
            Generator random = new Generator(Complexity, Type);
            Width = map.GetLength(1);
            Height = map.GetLength(0);
            Tiles = new Tile[Height, Width];

            for (var i = 0; i < Height; i++)
            {
                for (var j = 0; j < Width; j++)
                {
                    if (map[i, j] == 1)
                    {
                        Tiles[i, j] = new Tile(random.GetRandom());
                    } 
                    else
                    {
                        Tiles[i, j] = new Tile(8);
                    }
                }
            }
        }

        public Dictionary<Position, Move> GetMoves(Position start)
        {
            var list = new Dictionary<Position, Move>();
            for (var i = -2; i <= 2; i+=2)
            {
                for (var j = -2; j <= 2; j+=2)
                {
                    if (i == 0 && j == 0)
                        continue;
                    Position end = new Position(start.Row + i, start.Col + j);
                    Move move = Validate(start, end);
                    if (move != null)
                        list.Add(end, move);
                }
            }
            return list;
        }

        //Main game logic
        public Move Validate(Position from, Position to)
        {
            if (!TilesExist(from, to))
                return null;
            Tile start = Tiles[from.Row, from.Col];
            Tile middle = Tiles[(from.Row + to.Row) / 2, (from.Col + to.Col) / 2];
            Tile end = Tiles[to.Row, to.Col];
            if (middle.IsEmpty())
                return null;

            if (start.IsPrimary() && (end.IsPrimary() || end.DontContain(start)))
            {
                if (middle.IsPrimary() || middle.Contains(start))
                    return new Move(from, to);
            }
            if (start.IsSecondary() && middle.Contains(start))
            {
                if (end.DontContain(start) || end.IsEqual(start))
                    return new Move(from, to);
            }
            if (start.IsTertiary() && middle.IsTertiary() && (end.IsTertiary() || end.IsEmpty()))
                return new Move(from, to);
            return null;
        }

        private bool TilesExist(Position start, Position end)
        {
            if (start.Row < 0 || start.Row >= Height || start.Col < 0 || start.Col >= Width)
                return false;
            if (end.Row < 0 || end.Row >= Height || end.Col < 0 || end.Col >= Width)
                return false;
            if ((Math.Abs(start.Row - end.Row) != 2 && Math.Abs(start.Row - end.Row) != 0) 
                || (Math.Abs(start.Col - end.Col) != 2 && Math.Abs(start.Col - end.Col) != 0))
                    return false;
            if ((start.Row + end.Row) % 2 == 1 || (start.Col + end.Col) % 2 == 1)
                return false;
            if (Tiles[start.Row, start.Col].Value == 8 || Tiles[end.Row, end.Col].Value == 8 ||
                Tiles[(start.Row + end.Row) / 2, (start.Col + end.Col) / 2].Value == 8)
                return false;
            return true;
        }

        public bool IsFailed()
        {
            for (var i = 0; i < Height; i++)
            {
                for (var j = 0; j < Width; j++)
                {
                    if (Tiles[i, j] == null)
                        continue;
                    if (Tiles[i, j].Value == 0) continue;
                    if (GetMoves(new Position(i, j)).Count != 0)
                        return false;
                }
            }
            return true;
        }

        public bool IsSolved()
        {
            return TileNumber == 1;
        }
    }
}
