﻿using System;
using System.Collections.Generic;

namespace Tesserae
{
    [Serializable]
    public class History
    {
        [Serializable]
        private class Node
        {
            public Move Move { set; get; }
            public Node Next { set; get; }
            public Node Prev { set; get; }
        }

        private Node current;
        private readonly Node first;

        public int MoveNumber { get; set; }

        public History()
        {
            first = new Node();
            current = first;
        }

        public void Undo(Board board)
        {
            if (current.Prev != null)
            {
                current = current.Prev;
                current.Move.UndoMove(board);
                MoveNumber--;
            }
        }

        public void Redo(Board board)
        {
            if (current.Next != null)
            {
                current.Move.Execute(board);
                current = current.Next;
                MoveNumber++;
            }
        }

        public void AddMove(Move move)
        {
            move.Order = MoveNumber;
            current.Move = move;
            current.Next = new Node { Prev = current };
            current = current.Next;
            MoveNumber++;
        }

        /*This function creates new entities of Move
         * in order to avoid overwriting older saves*/
        public List<Move> SaveMoves()
        {
            var list = new List<Move>();
            Node temp = first;
            for (int i = 0; i < MoveNumber; i++)
            {
                Move move = new Move();
                move.Direction = temp.Move.Start.Col + " " +
                                      temp.Move.Start.Row + " " +
                                      temp.Move.End.Col + " " +
                                      temp.Move.End.Row;
                move.OldEnd = temp.Move.OldEnd;
                move.OldStart = temp.Move.OldStart;
                move.OldMiddle = temp.Move.OldMiddle;
                move.Order = temp.Move.Order;
                list.Add(move);
                temp = temp.Next;
            }
            return list;
        }

        public void LoadMoves(List<Move> moves)
        {
            moves.Sort((m, n) => m.Order - n.Order);
            foreach (Move move in moves)
            {
                string[] dir = move.Direction.Split(' ');
                move.Start = new Position(Int32.Parse(dir[1]), Int32.Parse(dir[0]));
                move.End = new Position(Int32.Parse(dir[3]), Int32.Parse(dir[2]));
                move.Middle = new Position(
                    (move.Start.Row + move.End.Row) / 2,
                    (move.Start.Col + move.End.Col) / 2
                );
                move.Direction = null;
                AddMove(move);
            }
        }
    }
}
