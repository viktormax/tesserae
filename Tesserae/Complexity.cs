﻿namespace Tesserae
{
    public enum Complexity
    {
        BEGINNER,
        INTERMEDIATE,
        ADVANCED
    }
}
