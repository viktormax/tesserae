﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using Tesserae;
using Tesserae.Services;

namespace ConsoleInterface
{
    public class ConsoleInterface
    {
        /*---------------------*/
        private Board board;
        private History history;
        /*---------------------*/
        private string name;
        private IScoreService scoreService = new ScoreServiceEF();
        private IRatingService ratingService = new RatingServiceEF();
        private ICommentService commentService = new CommentServiceEF();
        private IGameSaveService gameSaveService = new GameSaveServiceEF();

        public void Run()
        {
            do
            {
                Console.Clear();
                Console.Write("Enter your name: ");
                name = Console.ReadLine();
                if (!LoadGame())
                {
                    Init(out int type, out int complex);
                    board = new Board((MapType)type - 1, (Complexity)complex - 1);
                    history = new History();
                }

                while (!board.IsFailed())
                {
                    Console.Clear();
                    PrintBoard();
                    try
                    {
                        ProcessInput();
                    }
                    catch
                    {
                        break;
                    }
                }

                Console.Clear();
                PrintBoard();
                Console.ForegroundColor = ConsoleColor.Blue;
                Console.WriteLine(board.IsSolved() ? "\n\n\tYOU WIN\n\n" : "\n\n\tGAME OVER\n\n");
                Console.ResetColor();
            } while (GameEnd());
        }

        private bool LoadGame()
        {
            if (!YesNoQuestion("Do you want to load game?"))
                return false;
            try
            {
                List<GameSave> saves = gameSaveService.GetSaves(name);
                if (saves.Count == 0)
                {
                    Console.WriteLine("Nothing to load");
                    return false;
                }
                int i = 1, input = -1;
                foreach (GameSave g in saves)
                    Console.Write($"{i++}) {g.GameName}\n");
                
                while (input < 1 || input > saves.Count)
                {
                    Console.Write("Enter number: ");
                    try
                    {
                        input = Convert.ToInt32(Console.ReadLine());
                    }
                    catch
                    {
                        Console.WriteLine("Try again");
                    }
                }
                history = saves[input - 1].GetHistory();
                board = saves[input - 1].GetBoard();
            }
            catch (GameSaveException e)
            {
                Console.WriteLine(e.Message);
            }
            return true;
        }

        private void SaveGame()
        {
            Console.Write("Enter game save name: ");
            string gameName = Console.ReadLine();
            try
            {
                gameSaveService.SaveGame(new GameSave(name, gameName, board, history));
                Console.WriteLine("Game saved successfully");
                Console.ReadKey();
            }
            catch
            {
                Console.WriteLine("Can not save game");
                Console.ReadKey();
            }
        }

        private bool GameEnd()
        {
            scoreService.AddScore(new Score(name, history, board));
            PrintScores();

            int star = 0;
            while (star < 1 || star > 5)
            {
                Console.Write("Enter your rating (1 - 5): ");
                try
                {
                    star = Convert.ToInt32(Console.ReadLine());
                    ratingService.SetRating(new Rating(name, star));
                }
                catch(Exception e)
                {
                    Console.WriteLine(e.Message);
                    Console.WriteLine("Try again");
                }
            }
            Console.WriteLine(string.Format("{0:Game rating: 0.00}", ratingService.GetAvarageRating()));
            if (YesNoQuestion("What about comment?"))
            { 
                Console.Write("Comment: ");
                string content = Console.ReadLine();
                commentService.AddComment(new Comment(name, content));
            }
            PrintComments();
            if (YesNoQuestion("Do you want start new game?"))
                return true;
            return false;
        }

        private bool YesNoQuestion(string question)
        {
            Console.Write(question + "(Y/N) ");
            string input = Console.ReadLine().ToLower();
            return input.Equals("y") || input.Equals("yes");
        }

        private void PrintComments()
        {
            int i = 1;
            foreach (Comment c in commentService.GetComments())
            {
                Console.WriteLine($"{i++}) Name:{c.Player}\n {c.Content}");
            }
        }

        private void PrintScores()
        {
            int i = 1;
            foreach(Score s in scoreService.GetTopScores())
            {
                Console.WriteLine($"{i++}) Name:{s.Player} Tiles:{s.Tiles} Moves:{s.Moves} Score:{s.Points}     {s.PlayedOn}");
            }
        }

        private void Init(out int type, out int complex)
        {
            type = 0; complex = 0;
            Console.WriteLine(
                "1 - BIGRECTAINGLE;\n" +
                "2 - RACTANGLE;\n" +
                "3 - BIG_CIRCLE;\n" +
                "4 - INFINITY;\n" +
                "5 - HEXAGON;\n" +
                "6 - CIRCLE;\n" +
                "7 - EIGHT;\n" +
                "8 - ZERO;\n" +
                "9 - X\n"
            );
            while (type < 1 || type > 9)
            {
                try
                {
                    Console.Write("Select map: ");
                    type = Convert.ToInt32(Console.ReadLine());
                }
                catch
                {
                    Console.WriteLine("Try again");
                }
            }

            Console.WriteLine("\n" +
                "1 - BEGINNER\n" +
                "2 - INTERMEDIATE\n" +
                "3 - ADVANCE\n"
            );
            while (complex < 1 || complex > 3)
            {
                try
                {
                    Console.Write("Select complexity: ");
                    complex = Convert.ToInt32(Console.ReadLine());
                }
                catch
                {
                    Console.WriteLine("Try again");
                }
            }
            Console.Clear();
        }

        private void ProcessInput()
        {
            Regex regex = new Regex(@"(\d+)([a-z])");

            Console.Write("> Enter start position: ");
            string input = GetString();
            if (input == null) return;

            Match match = regex.Match(input);
            if (match.Length == 0) return;
            int x = Convert.ToInt32(match.Groups[1].Value) - 1;
            int y = match.Groups[2].Value[0] - 'a';

            var list = board.GetMoves(new Position(x, y));
            if (list.Count == 0) return;
            PrintMoves(list);

            Console.Write("> Enter end position: ");
            input = GetString();
            if (input == null) return;

            match = regex.Match(input);
            if (match.Length == 0) return;
            x = Convert.ToInt32(match.Groups[1].Value) - 1;
            y = match.Groups[2].Value[0] - 'a';

            Move move = GetMove(list, x, y);
            if (move == null) return;
            history.AddMove(move);
            move.Execute(board);
        }

        private void PrintMoves(Dictionary<Position, Move> list)
        {
            var i = 1;
            foreach (var move in list)
            {
                Console.WriteLine($"{i++}. {move.Key.Row + 1}{(char)(move.Key.Col + 'a')}");
            }
        }

        private Move GetMove(Dictionary<Position, Move> list, int x, int y)
        {
            foreach (var move in list)
            {
                if (move.Key.Col == y && move.Key.Row == x)
                    return move.Value;
            }
            return null;
        }

        private string GetString()
        {
            string input = Console.ReadLine().ToLower();
            switch (input)
            {
                case "q":
                    throw new Exception();
                case "u":
                    history.Undo(board);
                    return null;
                case "r":
                    history.Redo(board);
                    return null;
                case "s":
                    SaveGame();
                    return null;
                case "l":
                    LoadGame();
                    return null;
                default:
                    return input;
            }
        }

        private void PrintBoard()
        {
            PrintHeader();
            Tile[,] tiles = board.Tiles;
            for (var i = 0; i < board.Height; i++)
            {
                Console.Write("    ");
                for (var j = 0; j < board.Width; j++)
                        Console.Write("----");
                Console.Write($"-\n {i + 1, 2} ");
                for (var j = 0; j < board.Width; j++)
                {
                    if (tiles[i, j].Value != 8)
                    {
                        Console.Write(tiles[i, j].Value != 0 ? $"| " : $"|   ");
                        PrintColor(tiles[i, j].Value);
                    }
                    else 
                        Console.Write("| # "); 
                }
                Console.WriteLine("|");
            }
            Console.Write("    ");
            for (int j = 0; j < board.Width; j++)
                Console.Write("----");
            Console.WriteLine("-\n");
        }

        private void PrintHeader()
        {
            Console.WriteLine($"Tiles number: {board.TileNumber}   Moves: {history.MoveNumber}\n");
            Console.Write("      ");
            for (var i = 0; i < board.Width; i++)
                Console.Write((char)('a' + i) + "   ");
            Console.WriteLine();
        }

        private void PrintColor(byte n)
        {
            switch (n)
            {
                case 1: Console.ForegroundColor = ConsoleColor.Red; break;
                case 2: Console.ForegroundColor = ConsoleColor.Blue; break;
                case 3: Console.ForegroundColor = ConsoleColor.DarkMagenta; break;
                case 4: Console.ForegroundColor = ConsoleColor.Green; break;
                case 5: Console.ForegroundColor = ConsoleColor.DarkYellow; break;
                case 6: Console.ForegroundColor = ConsoleColor.DarkCyan; break;
                case 7: Console.ForegroundColor = ConsoleColor.Gray; break;
            }
            if (n != 0) Console.Write($"{n} ");
            Console.ResetColor();
        }
    }
}
