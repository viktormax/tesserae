﻿using Tesserae;
using Tesserae.Services;

namespace ConsoleInterface
{
    class Game
    {
        public static void Main(string[] args)
        {
            new ConsoleInterface().Run();
        }
    }
}
