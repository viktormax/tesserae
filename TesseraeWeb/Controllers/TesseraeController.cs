﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Tesserae;
using TesseraeWeb.Models;

namespace TesseraeWeb.Controllers
{
    public class TesseraeController : Controller
    {
    
        [Route("")]
        [Route("index")]
        public IActionResult Index()
        {
            HttpContext.Session.SetObject("game", new Game());
            return View();
        }

        [Route("game")]
        public IActionResult Game(Complexity comp, MapType map)
        {
            Game game = (Game)HttpContext.Session.GetObject("game");
            if (game.Board == null)
            {
                game.CreateGame(map, comp);
                HttpContext.Session.SetObject("game", game);
            }
            return View(game);
        }

        [Route("aboutgame")]
        public IActionResult AboutGame()
        {
            return View("aboutgame");
        }

    }
}