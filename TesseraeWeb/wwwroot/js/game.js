﻿let moves;

sessionStorage.setItem("gameState", 0);
getComments();
getScores();
getRatings();
getState();

function getMoves(row, col) {
    getState();
    if(sessionStorage.getItem("gameState") != '0'){
        return;
    }
    reset();
    getFromServer("game/getmoves/" + row + ':' + col, function (m) {
        moves = m;
        for (let i = 0; i < moves.length; i++) {
            let element = document.getElementById(moves[i]["end"]["row"] + ":" + moves[i]["end"]["col"]);
            element.classList.add("blink");
            element.setAttribute('onclick', "makeMove(" + element.id.split(':') + ")");
        }
    });
}

function makeMove(row, col) {
    getState();
    if(sessionStorage.getItem("gameState") != '0'){
        return;
    }
    let data = moves.find(function (move) {
        return move["end"]["row"] === row && move["end"]["col"] === col;
    });

    sendToServer('game/makemove', data, function () {
        getFromServer('game/getboard', function(resp) {
            document.getElementById("board").innerHTML = resp;
            getState();
        })
    })
}

function undo() {
    getState();
    if(sessionStorage.getItem("gameState") != '0'){
        return;
    }
    sendToServer('game/undo', null, function () {
        getFromServer('game/getboard', function(resp) {
            document.getElementById("board").innerHTML = resp;
        })
    })
}

function redo() {
    getState();
    if(sessionStorage.getItem("gameState") != '0'){
        return;
    }
    sendToServer('game/redo', null, function () {
        getFromServer('game/getboard', function(resp) {
            document.getElementById("board").innerHTML = resp;
        })
    })
}

function reset() {
    let elements = document.getElementsByClassName("tile");
    for (let i = 0; i < elements.length; i++) {
        elements[i].classList.remove("blink");
        elements[i].setAttribute('onclick', "getMoves(" + elements[i].id.split(':') + ")");
    }
}

function getComments() {
    getFromServer('comment/', function(comments) {
        let c = document.getElementById("comments");
        for (let i = 0; i < comments.length; i++) {
            let date = new Date(comments[i]["commentedOn"]);
            let html = '<div class="comment" style="font-size: 16px;">';
            html += '<div class="date">' + date.getDate() + '.' + date.getMonth() + '.' + date.getFullYear() + '</div>';
            html += '<div class="player">' + comments[i]['player'] + '</div>';
            html += '<div class="text">' + comments[i]['content'] + '</div></div>';
            c.innerHTML += html;
        }
    })
}

function getRatings() {
    getFromServer('rating/', function(rating) {
        let c = document.getElementById("ratings");
        let r = Math.floor(rating);
        let html = "<p class='ratingText' style='text-align: center'>Average rating</p>";
        for (let i = 0; i < r; i++) {
            html += "<img style='height: 50px; width: 50px' src='../images/star.png'/>"
        }
        getFromServer('rating/'+ 'Viktor Maksym', function(rating) {
            let c = document.getElementById("ratings");
            let r = Math.floor(rating);
            html += "<p class='ratingText' style='text-align: center'>Your rating</p>";
            for (let i = 0; i < r; i++) {
                html += "<img onclick='setRating(" + i + ")' style='height: 50px; width: 50px' src='../images/star.png'/>"
            }
            for (let i = r; i < 5; i++) {
                html += "<img onclick='setRating(" + i + ")' style='height: 50px; width: 50px' src='../images/estar.png'/>"
            }
            c.innerHTML = html;
        })
    });
}

function setRating(int) {
    sendToServer('rating/',
        {
            ratedOn: new Date(),
            player: profile.getGivenName() + ' ' + profile.getFamilyName(),
            stars: int + 1
        },
        function () {
            getRatings()
        }
    );
}

function getState() {
    getFromServer('game/gamestate', function (resp) {
        if (resp === 1) {
            Swal.fire({
                title: 'You win!',
                width: 600,
                padding: '3em',
                backdrop: `
                  rgba(0,0,123,0.4)
                  url("images/nyan-cat.gif")
                  center left
                  no-repeat
                `
            })
        } else if (resp === -1) {
            Swal.fire('You lost')
        }
        if (resp !== 0 && sessionStorage.getItem("stored") !== 'true') {
            sendToServer('score/',
                profile.getGivenName() + ' ' + profile.getFamilyName(),
                function (res) {
                    location.reload();
                }
            );
            sessionStorage.setItem('stored', 'true');
        }
        sessionStorage.setItem("gameState", resp);
    })
}

function processForm() {
    if (!profile) {
        alert("To send comment you should by logged in");
        return;
    }
    sendToServer('comment/',
        {
            commentedOn: new Date(),
            player: profile.getGivenName() + ' ' + profile.getFamilyName(),
            content: document.getElementById("commentText").value
        },
        function (res) {
            getComments();
        }
    );
}

function getScores() {
    getFromServer('score/', function (scores) {
        let c = document.getElementById("scores");
        c.innerHTML = '<div class="topScores">Top Scores</div>';
        for (let i = 0; i < scores.length; i++) {
            c.innerHTML += '<div class="score"><span class="name enum">' + (i + 1) + '</span>' + scores[i]['player'] + ' ' + '<span class="name points">' + scores[i]['points'] + ' </span>';
        }
    })
}