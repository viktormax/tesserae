﻿let serverURL = window.location.protocol + "//" + window.location.host + '/';
let profile;


function onSignIn(googleUser) {
    profile = googleUser.getBasicProfile();
    document.getElementById('googleSign').style.display = "none";
    document.getElementById('logged').style.display = "block";
    document.getElementById('logged').innerHTML = '<p>Logged as: ' + profile.getGivenName() + ' ' + profile.getFamilyName() + '</p>';
    //gapi.auth2.getAuthInstance().disconnect();
}

function getFromServer(url, handler) {
    const request = new XMLHttpRequest();
    request.onreadystatechange = function () {
        if (this.readyState === 4 && this.status === 200) {
            handler(JSON.parse(this.responseText));
        }
    };
    request.open("GET", serverURL + "api/" + url, true);
    request.send();
}

function sendToServer(url, data, handler) {
    const request = new XMLHttpRequest();
    request.onreadystatechange = function () {
        if (this.readyState === 4 && this.status === 200) {
            if (handler != null) {
                handler();
            }
        }
    };
    request.open("POST", serverURL + "api/" + url, true);
    request.setRequestHeader('Content-Type', 'application/json; charset=UTF-8');
    request.send(JSON.stringify(data));
}