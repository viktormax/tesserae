﻿let complexity = -1;

sessionStorage.setItem("stored", 'false');

function selectComplexity(index) {
    let elements = document.getElementsByClassName("but");
    for (let i = 0; i < elements.length; i++) {
        elements[i].classList.remove("selected");
    }
    document.getElementById(index).classList.add("selected");
    complexity = index;
}

function selectMap(index) {
    if (complexity === -1) {
        Swal.fire({
            type: 'error',
            title: 'Oops...',
            text: 'Select complexity first',
        });
    } else {
        location.href = serverURL + 'game?comp=' + complexity + '&map=' + index;
    }
}

function play() {
    document.getElementById('map').style.display = "block";
    document.getElementById('play').style.display = "none";
}
