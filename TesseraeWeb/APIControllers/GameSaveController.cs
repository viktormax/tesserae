﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Tesserae.Services;

namespace TesseraeWeb.Controllers
{
    [Produces("application/json")]
    [Route("api/gamesave")]
    public class GameSaveController : Controller
    {
        private IGameSaveService _gameSaveService = new GameSaveServiceEF();

        [HttpGet("{player}")]
        public IEnumerable<GameSave> Get([FromRoute]string player)
        {
            return _gameSaveService.GetSaves(player);
        }

        [HttpPut]
        public void Put([FromBody]GameSave gameSave)
        {
            _gameSaveService.SaveGame(gameSave);
        }

        [HttpDelete]
        [Route("{player}/{game}")]
        public void Delete([FromRoute]string player, [FromRoute]string game)
        {
            _gameSaveService.DeleteSave(player, game);
        }
    }
}