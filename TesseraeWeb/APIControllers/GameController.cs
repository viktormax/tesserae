﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Tesserae;
using TesseraeWeb.Models;

namespace TesseraeWeb.APIControllers
{
    [Produces("application/json")]
    [Route("api/game")]
    public class GameController : ControllerBase
    { 
        [HttpGet("getmoves/{row}:{col}")]
        public IEnumerable<Move> GetMoves(int row, int col)
        {
            Game game = (Game)HttpContext.Session.GetObject("game");
            return game.Board.GetMoves(new Position(row, col)).Values;
        }

        [HttpPost("makemove")]
        public void MakeMove([FromBody]Move move)
        {
            Game game = (Game)HttpContext.Session.GetObject("game");
            game.History.AddMove(move);
            move.Execute(game.Board);
            HttpContext.Session.SetObject("game", game);
        }

        [HttpGet("getboard")]
        public string GetBoard()
        {
            Game game = (Game)HttpContext.Session.GetObject("game");
            return game.GetBoardHtml();
        }
        
        [HttpPost("undo")]
        public void Undo()
        {
            Game game = (Game)HttpContext.Session.GetObject("game");
            game.History.Undo(game.Board);
            HttpContext.Session.SetObject("game", game);
        }
        
        [HttpPost("redo")]
        public void Redo()
        {
            Game game = (Game)HttpContext.Session.GetObject("game");
            game.History.Redo(game.Board);
            HttpContext.Session.SetObject("game", game);
        }

        [HttpGet("gamestate")]
        public int GameState()
        {
            Game game = (Game)HttpContext.Session.GetObject("game");
            if (game.Board.IsSolved())
            {
                return 1;
            }
            else if (game.Board.IsFailed())
            {
                return -1;
            }
            return 0;            
        }
    }
}