﻿using Microsoft.AspNetCore.Mvc;
using Tesserae.Services;

namespace TesseraeWeb.Controllers
{
    [Route("api/rating")]
    [Produces("application/json")]
    public class RatingController : Controller
    {
        private IRatingService _ratingService = new RatingServiceEF();

        [HttpGet("{player}")]
        public int Get([FromRoute] string player)
        {
            return _ratingService.GetRating(player);
        }

        [HttpGet]
        public float GetAverage()
        {
            return _ratingService.GetAvarageRating();
        }

        [HttpPost]
        public void Put([FromBody]Rating rating)
        {
            _ratingService.SetRating(rating);
        }

        [HttpDelete]
        public void Delete()
        {
            _ratingService.ClearRatings();
        }
    }
}