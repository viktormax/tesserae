﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Tesserae.Services;

namespace TesseraeWeb.Controllers
{
    [Route("api/comment")]
    [Produces("application/json")]
    public class CommentController : ControllerBase
    {
        ICommentService _commentService = new CommentServiceEF();

        [HttpDelete("{id}")]
        public void Delete([FromRoute] int id)
        {
            _commentService.DeleteComment(id);
        }

        [HttpDelete]
        public void DeleteAll()
        {
            _commentService.ClearComments(); ;
        }

        [HttpGet]
        public IEnumerable<Comment> Get()
        {
            return _commentService.GetComments();
        }

        [HttpPost]
        public void Put([FromBody] Comment comment)
        {
            _commentService.AddComment(comment);
        }
    }
}