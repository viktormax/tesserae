﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Tesserae.Services;
using TesseraeWeb.Models;

namespace TesseraeWeb.Controllers
{
    [Route("api/score")]
    [Produces("application/json")]
    public class ScoreController : Controller
    {
        private IScoreService _scoreService = new ScoreServiceEF();

        [HttpGet]
        public IEnumerable<Score> Get()
        {
            return _scoreService.GetTopScores();
        }

        [HttpPost]
        public void Post([FromBody]string name)
        {
            Game game = (Game)HttpContext.Session.GetObject("game");
            _scoreService.AddScore(new Score(name, game.History, game.Board));
        }

        [HttpDelete]
        public void Delete()
        {
            _scoreService.ClearScores();
        }
    }

}