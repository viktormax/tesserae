﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tesserae;

namespace TesseraeWeb.Models
{
    [Serializable]
    public class Game
    {
        public Board Board { get; set; }
        public History History { get; set; }

        public Game() { }

        public void CreateGame(MapType type, Complexity complexity)
        {
            Board = new Board(type, complexity);
            History = new History();
        }

        public string GetBoardHtml()
        {
            string html = "";
            for (var row = 0; row < Board.Height; row++)
            {
                html += "<tr>";
                for (var col = 0; col < Board.Width; col++)
                {
                    html += "<td>";
                    Tile tile = Board.Tiles[row, col];
                    if (tile.Value != 8)
                    {
                        html += "<div id ='" + row + ":" + col + "' class='tile type" + tile.Value + "' onclick='getMoves(" + row + "," + col + ")'></div>";
                    }
                    html += "</td>";
                }
                html += "</tr>";
            }
            return html;
        }
    }
}
